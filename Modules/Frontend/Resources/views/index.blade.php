@extends('frontend::layouts.master')

@section('content')
        <!--End of News Part-->
        <div class="left_advertisement">
            <img src="{{asset('frontend/image/left_advertisement_2.gif')}}" alt="" height="100px" width="90%">
        </div>

<!--Important News-->
        <div class="important_news" id="one" style="padding-top:80px;">
            <div class="main_news">
                <h5 style="font-weight: bold;">मुख्य समाचार <a href="#"><span style="float:right; padding-right:10px; color: white;">सबै</span></a></h5>
            </div>
            <h1>राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h1>
            <div class="post__info">
                <i class="fab fa-lyft"></i>
                <span>चिरञ्जीवी पौडेल</span>
                <i class="far fa-clock"></i>
                <span>२ घन्टा अगाडि</span>
                <i class="far fa-comment-alt"></i>
                <span>1</span>
            </div>
            <div class="hasImg">
                <img src="{{asset('frontend/image/news_5.jpg')}}" alt="" width="100%" height="550px">
            </div>
            <p>२२ असार, काठमाडौं । एक समयका क्रान्तिकारी कम्युनिष्ट नेता सीपी मैनालीको छवि र हैसियत अहिले निकै फेरिएको छ । उनले राष्ट्रवादी वामपन्थी नेताको छवि बनाएका छन् । तर, उनको राजनीतिक शक्ति भने निकै क्षीण भइसकेको छ । मैनाली नेकपा मालेका महासचिव हुन् । तर, उनको पार्टीले गत संसदीय चुनावमा थ्रेसहोल्ड कटाउन नसकेपछि राष्ट्रिय पार्टीको दर्जा […]</p>
        </div>
        <div class="important_left_advertisement">
            <img src="{{asset('frontend/image/left_advertisement_2.gif')}}" alt="" height="100px" width="90%">
        </div>
            
            <div class="important">
                <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_1.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_2.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_3.png')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_4.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_5.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="{{asset('frontend/image/news_6.jpg')}}" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="image/news_7.jpg" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-6">
                                <div class="important_news_image">
                                    <img src="image/news_8.jpg" alt="" width="100%" height="100px">
                                </div>
                            </div>
                            <div class="col-lg-7 col-6">
                                <div class="word">
                                    माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                    <p style="font-size: 13px; opacity: 0.7;">
                                        <i class="far fa-clock" style="color: red;"></i>
                                        <span style="padding-left: 8px;" >१४ घन्टा</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--Important News End-->
        <!--Post NEWs-->
        <div class="main_news">
            <h5 style="font-weight: bold;">मुख्य समाचार <a href="#"><span style="float:right; padding-right:10px; color: white;">सबै</span></a></h5>
        </div>
        <div class="post_news">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-12">
                        <div id="first_video">
                            <img src="image/news_6.jpg" alt="" width="100%" height="400px">
                            <h3 style="text-align: center">राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h3>
                            <div class="post__info">
                                <i class="fab fa-lyft"></i>
                                <span>चिरञ्जीवी पौडेल</span>
                                <i class="far fa-clock"></i>
                                <span>२ घन्टा अगाडि</span>
                                <i class="far fa-comment-alt"></i>
                                <span>1</span>
                            </div>
                        </div>
                        <div id="second_video" style="display: none;">
                            <img src="image/news_7.jpg" alt="" width="100%" height="400px">
                            <h3 style="text-align: center">राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h3>
                            <div class="post__info">
                                <i class="fab fa-lyft"></i>
                                <span>चिरञ्जीवी पौडेल</span>
                                <i class="far fa-clock"></i>
                                <span>२ घन्टा अगाडि</span>
                                <i class="far fa-comment-alt"></i>
                                <span>1</span>
                            </div>
                        </div>
                        <div id="third_video" style="display: none;">
                            <img src="image/news_8.jpg" alt="" width="100%" height="400px">
                            <h3 style="text-align: center">राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h3>
                            <div class="post__info">
                                <i class="fab fa-lyft"></i>
                                <span>चिरञ्जीवी पौडेल</span>
                                <i class="far fa-clock"></i>
                                <span>२ घन्टा अगाडि</span>
                                <i class="far fa-comment-alt"></i>
                                <span>1</span>
                            </div>
                        </div>
                        <div id="fourth_video" style="display: none;">
                            <img src="image/news_9.jpeg" alt="" width="100%" height="400px">
                            <h3 style="text-align: center">राजा महेन्द्रको उचित मूल्यांकन भएन : सीपी मैनाली</h3>
                            <div class="post__info">
                                <i class="fab fa-lyft"></i>
                                <span>चिरञ्जीवी पौडेल</span>
                                <i class="far fa-clock"></i>
                                <span>२ घन्टा अगाडि</span>
                                <i class="far fa-comment-alt"></i>
                                <span>1</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-12">
                        <div class="first_video">
                            <div class="row">
                                <div class="col-lg-6 col-6">
                                    <div class="important_news_image">
                                        <img src="image/news_6.jpg" alt="" width="100%" height="100px">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-6">
                                    <div class="word">
                                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                        <p style="font-size: 13px; opacity: 0.7;">
                                            <i class="far fa-clock" style="color: red;"></i>
                                            <span style="padding-left: 8px;" >१४ घन्टा</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="second_video">
                            <div class="row">
                                <div class="col-lg-6 col-6">
                                    <div class="important_news_image">
                                        <img src="image/news_7.jpg" alt="" width="100%" height="100px">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-6">
                                    <div class="word">
                                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                        <p style="font-size: 13px; opacity: 0.7;">
                                            <i class="far fa-clock" style="color: red;"></i>
                                            <span style="padding-left: 8px;" >१४ घन्टा</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="third_video">
                            <div class="row">
                                <div class="col-lg-6 col-6">
                                    <div class="important_news_image">
                                        <img src="image/news_8.jpg" alt="" width="100%" height="100px">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-6">
                                    <div class="word">
                                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                        <p style="font-size: 13px; opacity: 0.7;">
                                            <i class="far fa-clock" style="color: red;"></i>
                                            <span style="padding-left: 8px;" >१४ घन्टा</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fourth_video">
                            <div class="row">
                                <div class="col-lg-6 col-6">
                                    <div class="important_news_image">
                                        <img src="image/news_9.jpeg" alt="" width="100%" height="100px">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-6">
                                    <div class="word">
                                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको
                                        <p style="font-size: 13px; opacity: 0.7;">
                                            <i class="far fa-clock" style="color: red;"></i>
                                            <span style="padding-left: 8px;" >१४ घन्टा</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Post NEWs End-->

           <!--Third News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">अर्थ</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_5.jpg" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_6.JPG" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="image/left_advertisement_5.gif" alt="" height="100px" width="90%">
        </div>
        <!--End of Third News Start-->
        <!--Fourth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;"> खेलकुद</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_7.jpg" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_8.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="image/left_advertisement_6.gif" alt="" height="100px" width="90%">
        </div>
        <!--End of Fourth News Start-->
        <!--Fifth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">मनोरञ्जन</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_9.jpeg" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_10.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="left_advertisement">
            <img src="image/left_advertisement_7.gif" alt="" height="100px" width="90%">
        </div>
        <!--End of Fifth News Start-->
        <!--Sixth News Start-->
        <div class="main_news">
            <h5 style="font-weight: bold;">अन्य</h5>
        </div>
        <div class="main_news_item">
            <div class="item_image">
                <img src="image/news_11.jpg" alt=""  height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187);font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
            <div class="item_image">
                <img src="image/news_12.jpg" alt="" height="350px" width="100%">
            </div>
            <div class="item_text">
                <h4 style="color: rgb(56, 36, 187); font-weight: bold;">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</h4>
                <h6>2019-05-07, 11:00 am | src:NRSS | Ram Saran Thapa</h6>
                <p>६ असार, काठमाडौं । पूर्व प्रधानमन्त्री डा. बाबुराम भट्टराईले गुठी विधेयकविरुद्ध माइतीघर मण्डलामा भएको विशाल प्रदर्शनको संकेत बुझ्न सरकारलाई आग्रह गरेका छन् । 
                        माइतीघरमा ससुराली खलकले अगुल्टो उचालेको भन्दै उनले सतर्क रहन ज्वाइँमन्त्रीलाई आग्रह गरेका छन् । 
                        शुक्रबार संसदमा बोल्दै भट्टराईले भने,
                </p>
                <div class="head_btn_last">
                    <a href="#"><i class="fas fa-share-alt" style="color: black;"></i></a>
                    <button class="btn_last">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>

        <!--End of Sixth News Start-->
    </div>
    <!--End of Left advertisement news-->
    <!--right advertisement-->
    <div class="right">
        <button class="btn">थप समाचारहरु</button>
        <div class="right_advertisement" style="padding-top: 10px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_1.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_2.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_3.png" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_1.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_2.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_3.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_4.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_5.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_6.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_4.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_5.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_6.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_7.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_8.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_9.jpeg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_7.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_8.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement_gif">
                <img src="image/right_advertisement_9.gif" class="rounded" alt="" width="100%" height="250px">
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_10.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_11.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
        <div class="right_advertisement" style="padding-top: 5px;">
            <div class="advertisement">
                <h6>भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभ...</h6>
                <img src="image/news_12.jpg" class="rounded" alt="" width="100%" height="180px" style="padding: 0px 5px 5px 5px;">
                <div class="btn_value">
                    <button class="btn_2">पुरा पढ्नुहोस्</button>
                </div>
            </div>
        </div>
    </div>
    <!--End of right advertisement-->
</div>

@stop
