<html lang="en">

    @include('frontend::includes.head')
    <body>
        @include('frontend::includes.header')


        @if(request()->is('/'))
           
           @include('frontend::includes.leftnews')
        @endif
        
        
        @if(request()->is('/'))
           @include('frontend::includes.slider')
        @endif

        @yield('content')
        
        @include('frontend::includes.footer')
    </body>
</html>