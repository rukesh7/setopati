<div class="section_part" style="margin-top: 10px;">
    <!--Left advertisement news-->
    <div class="left">
        <div class="main_heading" style="padding-left:90px; padding-bottom: 4px;">
        <h2>अहिले रोक्का जग्गा माधव नेपाल सरकारको पालामा क्षेत्र विस्तार गर्दाको</h2>
        </div>
        <!--News Part-->
        <div class="news_part">
            <!--First News-->
            <div id="news_one" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_1.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of First News-->
            <!--Second News-->
            <div id='news_two' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_2.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of Second News-->
            <!--third News-->
            <div id='news_three' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_3.png')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of third News-->
            <!--fourth News-->
            <div id='news_four' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_4.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of fourth News-->
            <!--fifth News-->
            <div id='news_five' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_5.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of fifth News-->
            <!--sixth News-->
            <div id='news_six' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_6.JPG')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of sixth News-->
            <!--seventh News-->
            <div id='news_seven' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_7.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of seventh News-->
            <!--eighth News-->
            <div id='news_eight' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_8.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of eighth News-->
            <!--ninth News-->
            <div id='news_nine' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_9.jpeg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of ninth News-->
            <!--tenth News-->
            <div id='news_ten' style="display:none;" class="news_head">
                <div class="head_image">
                    <img src="{{asset('frontend/image/news_10.jpg')}}" alt="" height="433px" width="100%">
                </div>
                <div class="head_text">
                    <p>राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                        राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज भारतमा रासनकार्ड सुविधा, नेपालमा वृद्धभत्ता
                    </p>
                    <div class="head_btn">
                        <a href="#"><i class="fas fa-share-alt"></i></a>
                        <button class="btn_3">पुरा पढ्नुहोस्</button>
                    </div>
                </div>
            </div>
            <!--End of tenth News-->