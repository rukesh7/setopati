<!--Main Header-->
<header>      
    <div class="main_header" >
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-6  social_media">
                    <div class="social_item" style="padding-top: 8px;">
                        <a href=""><i class="fab fa-facebook-f text-center social rounded-circle"></i></a> 
                        <a href=""><i class="fab fa-twitter text-center social rounded-circle"></i></a>
                        <a href=""><i class="fab fa-youtube text-center social rounded-circle"></i></a>
                        <a href=""><i class="fab fa-google-plus-g text-center social rounded-circle"></i></a>
                        <a href=""><i class="fab fa-android text-center social rounded-circle"></i></a> 
                    </div>
                </div>
                <div class=" col-lg-3 col-md-4 col-sm-6  col-5 text-center">
                    <div class="telephone">
                        <div class="telephone_image">
                        <a href="#"><img src="{{asset('frontend/image/icon_telephone.png')}}" alt="" height="40px" width="40px"></a>
                        </div>
                        <div class="telephone_text">
                            <h5>9865134319</h5>
                            <h5>01-6610881</h5>
                        </div>
                    </div>
                </div>
                <div class=" col-lg-6 col-md-5 col-sm-12  col-12 text-center search" style="padding-top:5px;">
                    <input class="search_button" type="text" placeholder="Search any keywords...">
                    <span><a href="#"><i class="fas fa-search" style="color: white;"></i></a></span>
                </div>
            </div>
        </div>          
    </div> 
    <div class="second_header">
            <div class="row">
                <div class="col-lg-2 col-3">
                    <img src="{{asset('frontend/image/logo.jpg')}}" alt="" height="100px" width="100px">
                </div>
                <div class="col-lg-9 col-9 second_header_image">
                    <img src="{{asset('frontend/image/left_advertisement_1.gif')}}" alt="" height="100px" width="900px">
                </div>
            </div>
    </div>
</header>
<!--End of Main Header-->
<!-- First Nav Bar-->
<nav id="navbar" class="navbar navbar-expand-md navbar-dark" style="background-color:rgb(14, 4, 75); color: red;">
    <a href="" class="navbar-brand logo"><i class="fas fa-home"></i></a>
    <button class="navbar-toggler" type="button" style="background-color: red;" data-toggle="collapse" data-target="#collapsenavbar">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse text-center" id="collapsenavbar">
        <ul class="navbar-nav" >
            <li class="nav-item active">
                <a class="nav-link" style="color: red;" href="Samachar.html">मुख्य समाचार<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: red;">प्रदेश</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="background-color:rgb(14, 4, 75);">
                    <a class="dropdown-item" href="#" style="color: red;">......</a>
                    <a class="dropdown-item" href="#">......</a>
                    <a class="dropdown-item" href="#">......</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: red;" href="#">राजनीतिक</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: red;" href="#">अर्थ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: red;" href="#">खेलकुद</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: red;" href="#">मनोरञ्जन</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color: red;" href="#">अन्य</a>
            </li>
        </ul>
    </div>
</nav>  
<!--End of First Nav Bar-->
<!-- Second Nav Bar-->
<div id="owl-carousel-first" class="owl-carousel owl-theme second_nav_bar">
    <div class="item" >
        <a href="index.html" class="second">ताजा खबर</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">पञ्चेबाजासहित प्रचण्डको ५० औं वैवाहिक वर्षगाँठ</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">सडक धुलाम्मे भए पछि बौद्ध-जोरपाटी खण्डमा अवरोध</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">राहुलको नागरिकता सम्बन्धी मुद्दा सर्वोच्चद्वारा खारेज</a>
    </div>
    <div class="item">
        <a href="index.html" class="second">भारतमा रासनका सुविधा, नेपालमा वृद्धभ</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">पञ्चेबाजासहित प्रचण्डको ५० औं वैवाहिक वर्षगाँठ</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">सडक धुलाम्मे भए पछि</a>
    </div>
    <div class="item">
        <a href="index.html" class="second">नेपालमा वृद्धभ सडक धुलाम्मे भए पछि</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">भारतमा रासनका सुविधा, नेपालमा वृद्धभ</a>
    </div>
    <div class="item" >
        <a href="index.html" class="second">पञ्चेबाजासहित प्रचण्डको</a>
    </div>
</div>
<!--  End of Second Nav Bar-->
