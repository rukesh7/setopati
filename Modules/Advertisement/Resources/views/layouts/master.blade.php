<!DOCTYPE html>
<html>
<head>
    @include('advertisement::include.head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
        @include('advertisement::include.navbar')
        @include('advertisement::include.sidebar')
        @yield('content')
        @include('advertisement::include.footer')
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor');
    </script>
</div>
</body>