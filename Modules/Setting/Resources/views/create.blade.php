@extends('dashboard::layouts.master')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Company Info</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/dashboard/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Company Detail</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{Route('company.info')}}" method="post" enctype="multipart/form-data" >
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Company</label>
                    <input type="text" name="company" class="form-control" id="exampleInputEmail1" placeholder="Enter Company">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Address</label>
                    <input type="text" name="address" class="form-control" id="exampleInputPassword1" placeholder="Address">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Contact</label>
                    <input type="text" name="contact" class="form-control" id="exampleInputPassword1" placeholder="Contact">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <input type="email" name="email" class="form-control" id="exampleInputPassword1" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Register NUmber</label>
                    <input type="text" name="reg"  class="form-control" id="exampleInputPassword1" placeholder="Register Number">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Prakashak</label>
                    <input type="text" name="prakashak" class="form-control" id="exampleInputPassword1" placeholder="Prakashak">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Sampadakh</label>
                    <input type="text" name="sampadakh" class="form-control" id="exampleInputPassword1" placeholder="Sampadakh">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Year</label>
                    <input type="text" name="year" class="form-control" id="exampleInputPassword1" placeholder="Year">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Google Map</label>
                    <input type="text" name="GoogleMap" class="form-control" id="exampleInputPassword1" placeholder="Google Map">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Facebook</label>
                    <input type="text" name="facebook" class="form-control" id="exampleInputPassword1" placeholder="Facebook">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Youtube</label>
                    <input type="text" name="youtube" class="form-control" id="exampleInputPassword1" placeholder="Youtube">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Twitter</label>
                    <input type="text" name="twitter" class="form-control" id="exampleInputPassword1" placeholder="Twitter">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Google Plus</label>
                    <input type="text" name="GooglePlus" class="form-control" id="exampleInputPassword1" placeholder="Google Plus">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Caption</label>
                    <input type="text" name="caption" class="form-control" id="exampleInputPassword1" placeholder="Caption">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Keyword</label>
                    <input type="text" name="keyword" class="form-control" id="exampleInputPassword1" placeholder="Keyword">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Meta Tag</label>
                    <input type="text" name="MetaTag" class="form-control" id="exampleInputPassword1" placeholder="Meta Tag">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Meta Discription</label>
                    <input type="text" name="MetaDec" class="form-control" id="exampleInputPassword1" placeholder="Meta Discription">
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->



          </div>
</div></div></div>  

<script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('dashboard/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dashboard/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dashboard/dist/js/demo.js')}}"></script>
</body>
</html>
@endsection