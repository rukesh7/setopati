@extends('dashboard::layouts.master')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Company Info</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/dashboard/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<div class="content-wrapper">
@foreach($data as $datas)
<a href="{{Route('edit.company.info',$datas->id)}}" class="btn btn-primary btn-lg float-right m-3  ">edit</a>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Company Profile</h3>
              </div>
                        <table class="table">
                          <tbody>
                            <tr>
                              <th scope="row">Company Name</th>
                              <td>{{$datas->company}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Address</th>
                              <td>{{$datas->address}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Contact</th>
                              <td>{{$datas->contact}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Email</th>
                              <td>{{$datas->email}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company logo</th>
                            
                              <td><img src="{{asset('uploads/companylogo/'.$datas->image)}}"  width="100px" height="100px" alt="image"></td>
                            </tr>
                            <tr>
                              <th scope="row">Company Registration Number</th>
                              <td>{{$datas->regdno}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Prakashak</th>
                              <td>{{$datas->prakashak}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Sampadak</th>
                              <td>{{$datas->sampadak}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Establish Year</th>
                              <td>{{$datas->year}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Google Map</th>
                              <td>{{$datas->googleMap}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Facebook Link</th>
                              <td>{{$datas->facebook}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Youtube Link</th>
                              <td>{{$datas->youtube}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Twitter Link</th>
                              <td>{{$datas->twitter}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Google Plus Link</th>
                              <td>{{$datas->googlePlus}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Caption</th>
                              <td>{{$datas->caption}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company keyword</th>
                              <td>{{$datas->keyword}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Meta Tags</th>
                              <td>{{$datas->metaTag}}</td>
                            </tr>
                            <tr>
                              <th scope="row">Company Meta Discription</th>
                              <td>{{$datas->metaDesc}}</td>
                            </tr>
                          
                          </tbody>
                        </table>
             


              @endforeach
            </div>
          </div>
        </div>
      </div>  
    </select>
</div>

<script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('dashboard/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dashboard/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dashboard/dist/js/demo.js')}}"></script>
</body>
</html>
        

@endsection
