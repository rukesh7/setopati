@extends('dashboard::layouts.master')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Company Info</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/dashboard/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Company Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                          <form action="{{Route('update.company.info',$setting->id)}}" method="post" enctype="multipart/form-data" >
                          @csrf
                          {{method_field('PUT')}}
                            <div class="card-body">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Company</label>
                                <input type="text" name="company" value="{{$setting->company}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Company">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Address</label>
                                <input type="text" name="address" value="{{$setting->address}}" class="form-control" id="exampleInputPassword1" placeholder="Address">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Contact</label>
                                <input type="text" name="contact" value="{{$setting->contact}}" class="form-control" id="exampleInputPassword1" placeholder="Contact">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Email</label>
                                <input type="email" name="email" value="{{$setting->email}}" class="form-control" id="exampleInputPassword1" placeholder="Email">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                  <div class="custom-file">
                                    <input type="file" name="image" value="{{$setting->image}}" class="custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                  </div>
                                  <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Register NUmber</label>
                                <input type="text" name="reg" value="{{$setting->regdno}}"  class="form-control" id="exampleInputPassword1" placeholder="Register Number">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Prakashak</label>
                                <input type="text" name="prakashak" value="{{$setting->prakashak}}" class="form-control" id="exampleInputPassword1" placeholder="Prakashak">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Sampadakh</label>
                                <input type="text" name="sampadakh" value="{{$setting->sampadakh}}" class="form-control" id="exampleInputPassword1" placeholder="Sampadakh">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Year</label>
                                <input type="text" name="year" value="{{$setting->year}}" class="form-control" id="exampleInputPassword1" placeholder="Year">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Google Map</label>
                                <input type="text" name="GoogleMap" value="{{$setting->googleMap}}" class="form-control" id="exampleInputPassword1" placeholder="Google Map">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Facebook</label>
                                <input type="text" name="facebook" value="{{$setting->facebook}}" class="form-control" id="exampleInputPassword1" placeholder="Facebook">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Youtube</label>
                                <input type="text" name="youtube" value="{{$setting->youtube}}" class="form-control" id="exampleInputPassword1" placeholder="Youtube">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Twitter</label>
                                <input type="text" name="twitter" value="{{$setting->twitter}}" class="form-control" id="exampleInputPassword1" placeholder="Twitter">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Google Plus</label>
                                <input type="text" name="GooglePlus" value="{{$setting->googlePlus}}" class="form-control" id="exampleInputPassword1" placeholder="Google Plus">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Caption</label>
                                <input type="text" name="caption" value="{{$setting->caption}}" class="form-control" id="exampleInputPassword1" placeholder="Caption">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Keyword</label>
                                <input type="text" name="keyword" value="{{$setting->keyword}}" class="form-control" id="exampleInputPassword1" placeholder="Keyword">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Meta Tag</label>
                                <input type="text" name="MetaTag" value="{{$setting->metaTag}}" class="form-control" id="exampleInputPassword1" placeholder="Meta Tag">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Meta Discription</label>
                                <input type="text" name="MetaDec" value="{{$setting->metaDesc}}" class="form-control" id="exampleInputPassword1" placeholder="Meta Discription">
                              </div>
                              
                            <!-- /.card-body -->

                            <div class="card-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                          </form>

                          </div>
        </div>
      </div>  
    </select>
</div>


<script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('dashboard/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dashboard/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dashboard/dist/js/demo.js')}}"></script>
</body>
</html>
@endsection