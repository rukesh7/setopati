<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('company');
            $table->String('address');
            $table->String('contact');
            $table->String('email');
            $table->mediumText('image')->nullable();
            $table->String('regdno');
            $table->String('prakashak');
            $table->String('sampadak');
            $table->String('year');
            $table->String('googleMap');
            $table->String('facebook');
            $table->String('youtube');
            $table->String('twitter');
            $table->String('googlePlus');
            $table->String('caption');
            $table->String('keyword');
            $table->String('metaTag');
            $table->String('metaDesc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
