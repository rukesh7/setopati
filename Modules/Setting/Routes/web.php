<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('setting')->group(function() {
    Route::get('/profile','SettingController@show')->name('profile');
    Route::get('/editprofile/{id}','SettingController@edit')->name('edit.company.info');
    Route::put('/update/{id}','SettingController@update')->name('update.company.info');

});
