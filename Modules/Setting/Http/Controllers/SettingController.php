<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Entities\Setting;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
        
    
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        return view('setting::profile')->with('data', Setting::all()); 
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $setting=Setting::find($id);
        return view ('setting::update')->with('setting',$setting); 
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $setting=Setting::find($id);
        $setting->company=$request->input('company');
        $setting->address=$request->input('address');
        $setting->contact=$request->input('contact');
        $setting->email=$request->input('email');
        if($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $image->move(public_path('uploads/companylogo'), $filename);
            $setting->image = $request->file('image')->getClientOriginalName();
        }
        $setting->regdno=$request->input('reg');
        $setting->prakashak=$request->input('prakashak');
        $setting->sampadak=$request->input('sampadakh');
        $setting->year=$request->input('year');
        $setting->googleMap=$request->input('GoogleMap');
        $setting->facebook=$request->input('facebook');
        $setting->youtube=$request->input('youtube');
        $setting->twitter=$request->input('twitter');
        $setting->googlePlus=$request->input('GooglePlus');
        $setting->caption=$request->input('caption');
        $setting->keyword=$request->input('keyword');
        $setting->metaTag=$request->input('MetaTag');
        $setting->metaDesc=$request->input('MetaDec');
        $setting->save();
        return redirect(route('profile'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
