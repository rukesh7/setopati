<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table='settings';
    protected $fillable = ['company','address','contact','email','image','regd no','prakashak','sampadak','year','googleMap','facebook','youtube','twitter','googlePlus','caption','keyword','metaTag','metaDesc'];
}
